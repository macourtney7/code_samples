<?php
require ("/foo/bar/db_config.php");
if(isset($_POST['uhid']) && isset($_POST['row'])) {
  $conn=db_connection();
  if(!$conn->connection_errno) {
    $res=$conn->query("SELECT ".$_POST['uhid'].".x,".$_POST['uhid'].".y,".$_POST['uhid'].".pip_uhid,pip_img.href,pip_img.created FROM "
      .$_POST['uhid']." LEFT JOIN pip_img ON ".$_POST['uhid'].".pip_uhid=pip_img.uhid"
      ." WHERE ".$_POST['uhid'].".y=".strval($_POST['row']*120)." AND pip_img.href IS NOT NULL ORDER BY y ASC;");
    $conn->close();
    $row_output=$_POST['row'].";";
    while($relation=$res->fetch_assoc()) {
      $row_output=$row_output.$relation['x'].",".$relation['y'].","
        .$relation['pip_uhid'].",".$relation['href'].",".$relation['created']."|";
    }
    echo substr($row_output,0,-1);
  } else { echo '-1'; }
} else { echo '-1'; }
?>
