<?php
session_start();
if(isset($_FILES['file']) && getimagesize($_FILES['file']['tmp_name']) &&
  strtolower(pathinfo(basename($_FILES['file']['name']),PATHINFO_EXTENSION))=='png' ||
  strtolower(pathinfo(basename($_FILES['file']['name']),PATHINFO_EXTENSION))=='jpg' ||
  strtolower(pathinfo(basename($_FILES['file']['name']),PATHINFO_EXTENSION))=='jpeg' &&
  round(filesize($_FILES['file']['tmp_name'])/1000000,2)<3.0 && round(filesize($_FILES['file']['tmp_name'])/1000000,2)>0) {
    $target_fname=substr(md5(basename($_FILES['file']['name']).time()),0,6);
    $target_path='/foo/bar/'.$target_fname.'.png';

    if(move_uploaded_file($_FILES['file']['tmp_name'],$target_path)) {
      $orientation=exif_read_data($target_path)['Orientation'];

      if($orientation!=1){ //mobile_opti - correct orientation
        switch($orientation){
          case 3:
            $_SESSION['deg']=180;
            break;
          case 6:
            $_SESSION['deg']=90;
            break;
          case 8:
            $_SESSION['deg']=270;
            break;
          default:
            $_SESSION['deg']=0;
            break;
        }
        echo $target_fname."_".substr($_FILES['file']['name'],0,strlen($_FILES['file']['name'])-4)."_".$_SESSION['deg'];
      } else { echo '-1'; }
    }
} else if(isset($_POST['utmpid']) && isset($_POST['fname']) && isset($_POST['async']) && isset($_SESSION['deg'])) {
    $utmpid_f=$_POST['utmpid'].'.png';
    if(strtolower(pathinfo(basename($utmpid_f),PATHINFO_EXTENSION))=='png') {
      $exec_cmd="sudo -u foo /foo/bar/scripts/scp_pip_i ".$utmpid_f." ".$_POST['fname'].".png";
      if($_POST['async']=='1') { exec($exec_cmd." ".$_SESSION['deg']." > /dev/null &"); } //mobile_opti - don't wait for return
      else {
        exec($exec_cmd,$o);
        echo $o[0];
      }
      echo $_POST['utmpid'];
    } else { echo '-1'; }
} else if(isset($_FILES['0'])) {
    $verify=true;
    $upload_limit=0;
    for($i=0; $i<sizeof($_FILES); $i++) {
      if(!getimagesize($_FILES[$i]['tmp_name']) &&
        (strtolower(pathinfo(basename($_FILES[$i]['name']),PATHINFO_EXTENSION))!='png' ||
        strtolower(pathinfo(basename($_FILES[$i]['name']),PATHINFO_EXTENSION))!='jpg' ||
        strtolower(pathinfo(basename($_FILES[$i]['name']),PATHINFO_EXTENSION))!='jpeg')) {
          $verify=false;
          break;
      } else { $upload_limit+=round(filesize($_FILES[$i]['tmp_name'])/1000000,2); }
    }
    if($verify && $upload_limit>0 && $upload_limit<10.0) {
      $url_o="";
      $process_success=true;
      for($i=0; $i<sizeof($_FILES); $i++) {
        $utmpid_f=substr(md5(basename($_FILES[$i]['name']).time()),0,6).".png";
        $utmpid_dir="/foo/bar/utmp/".$utmpid_f;
        if(strtolower(pathinfo(basename($_FILES[$i]['name']),PATHINFO_EXTENSION))=='jpg' ||
          strtolower(pathinfo(basename($_FILES[$i]['name']),PATHINFO_EXTENSION))=='jpeg') {
            if(move_uploaded_file($_FILES['file']['tmp_name'],'/foo/bar/utmp/'.$target_fname.'.png')) { $url_o+="https://web.site/utmp/".$utmpid_f." "; }
            else { $process_success=false; }
        } else if(move_uploaded_file($_FILES['file']['tmp_name'],'/foo/bar/utmp/'.$target_fname.'.png')) { $url_o+="https://web.site/utmp/".$utmpid_f." "; }
        else { $process_success=false; }
      }
      if($process_success) { echo substr($url_o,0,-1); }
      else { echo '-1'; }
    } else { echo '-1'; }
} else if(isset($_POST['batch_files_metadata'])) {
    $metadata=explode(" ",$_POST['batch_files_metadata']);
    $url_o="";
    for($i=0; $i<sizeof($metadata); $i++){
      $tmp=explode(",",$metadata[$i]);
      $url_o=$url_o.exec("sudo -u foo /foo/bar/scp_pip_i ".$tmp[0].".png"." ".$tmp[1].".png")." ";
    }
    echo substr($url_o,0,-1);
} else { echo '-1'; }
?>
