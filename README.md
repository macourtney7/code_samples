### Matt Courtney - macourtney7@protonmail.ch ###
Welcome to my code samples repository!  

This is a very small selection of previous code I have written.  
  
Please find links to my [CV](docs/cv.md) and [LinkedIn](https://bit.ly/macourtney7)
### [Python](py) ###
  * [Flask Web Application](py/flask)
  * [OpenCV](py/img)
### [Haskell](hs) ###
  * [Misc](hs)
### [PHP](php) ###
  * [Core Web](php/core-web)
### [JavaScript](js) ###
  * [Asynchronous Uploader](js/async-uploader)
### [Misc](misc/) ###
  * [HTML](misc/web)
### Papers ###
  * [DL-based NLP Binary Analysis](https://bit.ly/ba_nlp)
  * [IEEE Point Cloud](https://bit.ly/ieee_paper)
