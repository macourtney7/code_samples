### Matt Courtney - Curriculum Vitae ###
macourtney7@protonmail.ch  
LinkedIn: [bit.ly/macourtney7](https://bit.ly/macourtney7)
### Statement ###
Software developer working in Haskell (Yesod), JavaScript (Vue), and C# to build a high-availabilty cloud-hosted system for the rail industry. I have a strong understanding of the foundations for successful remote and cross-disciplinary working with experience doing so from locations worldwide.
### Employment ###
#### 2020 - Present, Tracsis Plc ####
Haskell Software Developer (Remote)
### Qualifications ###
#### 2019 - 2020, Distance Learning ####
Machine Learning Applications on AWS (AWS Scholarship)  
Tezos Blockchain Developer Certification (Tezos Foundation Scholarship)  
IoT Edge Applications in OpenVINO (Intel Scholarship)  
#### 2016 - 2018, Cardiff University ####
Research Studentship, School of Engineering  
#### 2015 - 2016, Cardiff University ####
Master of Science, Advanced Computer Science
#### 2012 - 2015, Cardiff University ####
Bachelor of Science, Software Engineering
#### 2011 - 2012, Coleg Sir Gâr ####
Access to Higher Education (Annual Prize Winner) - Maths, Physics, Chemistry, Biology

_Note: Full CV available on LinkedIn_
