$(document).ready(function() {
    const guid = data.guid
    const resources = data.resources
    $('#registerAccountForm').on('submit', e => {
        e.preventDefault()
        toggleRegisterAccountForm(resources, true)
        $.ajax(
            { type: 'post',
              url: '/registerAccount/' + guid,
              contentType: 'application/json',
              data: registerAccountData(resources),
              success: r => {
                const resp = JSON.parse(r)
                if (resp.success) {
                    $('#registerAccountFormError').css('visibility', 'hidden')
                    $('#newAccountOTP').val(resp.newAccount.otp)
                    appendUser(resp.newAccount)
                    $('#clearRegisterAccount').css('display', 'inline')
                } else {
                    $('#registerAccountFormError').css('visibility', 'visible')
                    toggleRegisterAccountForm(resources, false)
                }
              },
            }
        )
    })

    $('#copyOTP').on('click', e => {
        e.preventDefault()
        if (!copyToClipboard($('#newAccountOTP').val())) {
            console.log("copy button failed")
        }
    })

    $('#clearRegisterAccount').on('click', e => {
        e.preventDefault()
        $('#clearRegisterAccount').css('display', 'none')
        clearRegisterAccountForm(resources)
        toggleRegisterAccountForm(resources, false)
    })

    $('#registerResourceForm').on('submit', e => {
        e.preventDefault()
        $.ajax(
            { type: 'post',
              url: '/registerResource/' + guid,
              contentType: 'application/json',
              data: registerResourceData(),
              success: r => {
                const resp = JSON.parse(r)
                if (resp.success) {
                    appendResource(resp.newResource)
                    clearResourceForm()
                } else {
                    $('#registerResourceFormError').css('visibility', 'visible')
                }
              },
            }
        )
    })

    $('.accountDelete').on('click', e => {
        e.preventDefault()
        $.ajax(
            { type: 'post',
              url: '/deleteAccount/' + guid,
              contentType: 'application/json',
              data: deleteAccountData(e.target.id),
              success: r => {
                const resp = JSON.parse(r)
                if (resp.success) {
                    $('#accountDeleteError').css('visibility', 'hidden')
                    location.reload()
                } else {
                    $('#accountDeleteError').css('visibility', 'visible')
                }
              },
            }
        )
    })

    $('.resourceDelete').on('click', e => {
        e.preventDefault()
        $.ajax(
            { type: 'post',
              url: '/deleteResource/' + guid,
              contentType: 'application/json',
              data: deleteResourceData(e.target.id),
              success: r => {
                const resp = JSON.parse(r)
                if (resp.success) {
                    $('#resourceDeleteError').css('visibility', 'hidden')
                    location.reload()
                } else {
                    $('#resourceDeleteError').css('visibility', 'visible')
                }
              },
            }
        )
    })
})

function attemptRegisterAccount() {
    return true
}

function attemptRegisterResource() {
    return true
}

function registerAccountData(resources) {
    return JSON.stringify(
        { newAccountUsername : $('#newAccountUsername').val(),
          newAccountResources : resourcePermissions(resources),
        }
    )
}

function resourcePermissions(resources) {
    let perms = { }
    for (const res of resources) {
        perms[res] = $('#cb' + res).is(":checked")
    }
    return perms
}

function registerResourceData() {
    return JSON.stringify(
        { newResourceName: $('#newResourceName').val(),
          newResourceCode: $('#newResourceCode').val().toUpperCase(),
          newResourceURL: $('#newResourceURL').val(),
          newResourceGSlidesURL: $('#newResourceGSlidesURL').val(),
        }
    )
}

function deleteAccountData(guid) {
    return JSON.stringify(
        { deleteAccountGuid: guid,
        }
    )
}

function deleteResourceData(guid) {
    return JSON.stringify(
        { deleteResourceGuid: guid,
        }
    )
}

function copyToClipboard(otp) {
   let isSuccess = false
   const textArea = document.createElement('textArea')
   textArea.value = otp
   document.body.appendChild(textArea)
   textArea.select()
   try {
      isSuccess = document.execCommand('copy')
   } catch (err) {
      console.log(err)
   }
   document.body.removeChild(textArea)
   return isSuccess
}

function appendUser(newAccount) {
    $('#userAccountsTable').append(' \
        <tr> \
            <th scope="row">' + newAccount.username + '</th> \
            <td>' + newAccount.resources + '</td> \
            <td>' + newAccount.last_accessed + '</td> \
            <td>' + newAccount.created + '</td> \
            <td> \
                <button type="button" class="accountDelete btn btn-danger" aria-label="Left Align" id="' + newAccount.guid + '" disabled> \
                    <span class="fas fa-user-minus" aria-hidden="true" id="' + newAccount.guid + '"></span> \
                </button> \
            </td> \
        </tr> \
    ')
}

function appendResource(newResource) {
    $('#resourcesTable').append(' \
        <tr> \
            <th scope="row">' + newResource.name + '</th> \
            <td>' + newResource.code + '</td> \
            <td>' + newResource.created + '</td> \
            <td> \
                <button type="button" class="resourceDelete btn btn-danger" aria-label="Left Align" id="' + newResource.guid + '" disabled> \
                    <span class="fas fa-trash" aria-hidden="true" id="' + newResource.guid + '"></span> \
                </button> \
            </td> \
        </tr> \
    ')
}

function clearResourceForm() {
    $('#registerResourceFormError').css('visibility', 'hidden')
    $('#newResourceName').val('')
    $('#newResourceCode').val('')
    $('#newResourceURL').val('')
    $('#newResourceGSlidesURL').val('')
}

function clearRegisterAccountForm(resources) {
    $('#newAccountUsername').val('')
    $('#newAccountOTP').val('')
    clearRegisterAccountCheckboxes(resources)
}

function clearRegisterAccountCheckboxes(resources) {
    for(const i in resources) {
        $('#' + resources[i]).prop('checked', false)
    }
}

function toggleRegisterAccountForm(resources, isDisabled) {
    $('#createRegisterAccount').prop('disabled', isDisabled)
    $('#newAccountUsername').prop('disabled', isDisabled)
    for (const i in resources) {
        $('#' + resources[i]).prop('disabled', isDisabled)
    }
}