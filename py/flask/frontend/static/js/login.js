$(document).ready(function() {
    $('#loginForm').on('submit', e => {
        e.preventDefault()
        $.ajax(
            { type: 'post',
              url: '/login',
              contentType: 'application/json',
              data: loginData(),
              success: r => {
                const resp = JSON.parse(r)
                if(resp.isOTP) {
                    $('#passwordChangeUsername').val(resp.username)
                    toggleDialog()
                } else if(resp.success) {
                    window.location.href = '/account/' + resp.guid
                } else {
                    $('#loginFormError').css('visibility', 'visible')
                }
              },
            }
        )
    })

    $('#passwordChangeForm').on('submit', e => {
        e.preventDefault()
        if (changePasswordFormValid()) {
            $.ajax(
                { type: 'post',
                  url: '/changePassword',
                  contentType: 'application/json',
                  data: passwordChangeData(),
                  success: r => {
                    const resp = JSON.parse(r)
                    if (resp.success) {
                        $('#passwordChangeFormError').css('visibility', 'hidden')
                        $('#loginFormUsername').val(resp.username)
                        $('#loginFormPassword').val('')
                        toggleDialog()
                    } else {
                        $('#passwordChangeFormError').html('Account was not found')
                        $('#passwordChangeFormError').css('visibility', 'visible')
                    }
                  },
                }
            )
        } else {
            e.stopPropagation()
        }
    })
})

function attemptLogin() {
    return true
}

function attemptPasswordChange() {
    return true
}

function changePasswordFormValid() {
    if ($('#passwordChangeNewA').val() !== $('#passwordChangeNew').val()) {
        $('#passwordChangeFormError').html('New passwords do not match')
        $('#passwordChangeFormError').css('visibility', 'visible')
        return false
    } else if ($('#passwordChangeNew').val() === $('#passwordChangePrev').val()) {
        $('#passwordChangeFormError').html('New passwords may not be the same as previous')
        $('#passwordChangeFormError').css('visibility', 'visible')
        return false
    }
    return true
}

function toggleDialog() {
    $('#loginDialog').toggleClass('hidden')
    $('#passwordChangeDialog').toggleClass('hidden')
}

function passwordChangeData() {
    return JSON.stringify(
        { passwordChangeUsername: $('#passwordChangeUsername').val(),
          passwordChangePrev: $('#passwordChangePrev').val(),
          passwordChangeNew: $('#passwordChangeNew').val(),
        }
    )
}

function loginData() {
    return JSON.stringify(
        { loginFormUsername: $('#loginFormUsername').val(),
          loginFormPassword: $('#loginFormPassword').val(),
        }
    )
}