import hashlib

from foo.pythonservice.types.ids import loginTokenNotExpired
from flask import Flask, render_template, json, request, session, redirect, url_for

import foo.pythonservice.handlers.login as Login
import foo.pythonservice.handlers.logout as Logout
import foo.pythonservice.handlers.register as Reg
import foo.pythonservice.handlers.resources as ResHandl
import foo.pythonservice.types.resources as ResTypes

app = Flask(__name__)

Reg.setup()

def data():
    return json.dumps(
        Config.data
    )

def adminAccountViewData(guid):
    return \
        { 'guid' : guid
        , 'resources' : ResTypes.allres().permittedGuidsList()
        }

def userAccountViewData(guid):
    return \
        { 'guid' : guid
        }

@app.route('/')
def index():
    userGuid = Login.guidFromSessionIfNotOTP(session)
    if userGuid is not None and \
        loginTokenNotExpired(Login.loginTokenFromSession(userGuid, session)):
            return redirect('account/' + str(userGuid))
    return render_template('login.html', data = data())

@app.route('/login', methods = ['POST'])
def login():
    req = request.get_json()
    return Login.loginResponse(
        Login.sessionWithLoginToken(session),
        Login.attemptLogin(
            req['loginFormUsername'],
            hashlib.sha224(bytes(req['loginFormPassword'], encoding = 'utf8')).hexdigest(),
        )
    )

@app.route('/registerAccount/<uuid:guid>', methods = ['POST'])
def registerAccount(guid):
    if Login.tokenIsAdmin(Login.loginTokenFromSession(str(guid), session)):
        req = request.get_json()
        return Reg.registerAccountResponse(Reg.attemptRegisterAccount(
            req['newAccountUsername'],
            req['newAccountResources'],
        ))
    return (json.dumps({ 'success' : False }), 200)

@app.route('/deleteAccount/<uuid:guid>', methods = ['POST'])
def deleteAccount(guid):
    if Login.tokenIsAdmin(Login.loginTokenFromSession(str(guid), session)):
        return Reg.deleteAccountResponse(Reg.attemptDeleteAccount(
            request.get_json()['deleteAccountGuid'],
        ))

@app.route('/changePassword', methods = ['POST'])
def changePassword():
    req = request.get_json()
    return Reg.passwordChangeResponse(Reg.attemptPasswordChange(
        req['passwordChangeUsername'],
        hashlib.sha224(bytes(req['passwordChangePrev'], encoding = 'utf8')).hexdigest(),
        hashlib.sha224(bytes(req['passwordChangeNew'], encoding = 'utf8')).hexdigest(),
    ))

@app.route('/account/<uuid:guid>')
def account(guid):
    accountView = Login.tokenCheck(Login.loginTokenFromSession(str(guid), session))
    if accountView is not None:
        return render_template(
            'account.html',
            data = json.dumps(adminAccountViewData(str(guid)) if accountView.isAdmin else userAccountViewData(str(guid))),
            account = json.loads(Login.accountViewData(accountView)),
        )
    return redirect(url_for('index'))

@app.route('/logout/<uuid:guid>')
def logout(guid):
    if Logout.attemptLogout(session) and 'loginToken' not in session:
        return redirect(url_for('index'))
    return redirect('account/' + str(guid))

@app.route('/registerResource/<uuid:guid>', methods = ['POST'])
def registerResource(guid):
    if Login.tokenIsAdmin(Login.loginTokenFromSession(str(guid), session)):
        req = request.get_json()
        return ResHandl.registerResourceResponse(ResHandl.attemptRegisterResource(
            req['newResourceName'],
            req['newResourceCode'],
            req['newResourceURL'],
            req['newResourceGSlidesURL']
        ))
    return (json.dumps({'success' : False}), 200)

@app.route('/deleteResource/<uuid:guid>', methods = ['POST'])
def deleteResource(guid):
    if Login.tokenIsAdmin(Login.loginTokenFromSession(str(guid), session)):
        return ResHandl.deleteResourceResponse(ResHandl.attemptDeleteResource(
            request.get_json()['deleteResourceGuid'],
        ))

@app.errorhandler(404)
def pageNotFound(error):
    return (render_template('404.html'), 404)
