from foo.pythonservice.types.ids import LoginToken

import foo.pythonservice.types.adminAccount as AA
import foo.pythonservice.types.resources as Res
import foo.pythonservice.types.teachersPackAccount as TPA

class ActiveUser:
    guid = None
    isAdmin = False
    loginToken = None
    def __init__(self, guid, account, isAdmin):
        self.guid = guid
        self.isAdmin = isAdmin
        self.loginToken = LoginToken(self.guid)
        if self.isAdmin:
            self.account = AA.AdminAccount(self.guid, account)
        elif account is not None:
            self.account = TPA.TeachersPackAccount(self.guid, account)
    def username(self):
        if self.account is not None:
            return self.account.username
        return None
    def expected(self):
        if self.account is not None:
            return self.account.expected
        return None
    def resources(self):
        if self.isAdmin:
            return Res.allres()
        elif self.account is not None:
            return self.account.resources
        return None
    def created(self):
        if self.account is not None:
            return self.account.created
        return None
    def last_accessed(self):
        if self.account is not None:
            return self.account.last_accessed
        return None
    def isOTP(self):
        if self.isAdmin:
            return False
        elif self.account is not None:
            return self.account.isOTP
        return False

def guidIfNotOTP(activeUser):
    if not activeUser.isOTP():
        return activeUser.guid
    return None
