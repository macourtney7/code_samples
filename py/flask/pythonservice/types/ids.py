import uuid, hashlib

from foo.pythonservice.handlers.backend import Database
from datetime import datetime, timedelta

import foo.pythonservice.handlers.backend as Backend

def newGuid():
    return str(uuid.uuid4())

def newToken():
    return hashlib.sha224(bytes(newGuid(), encoding = 'utf8')).hexdigest()[:8]

class LoginToken:
    created = None
    tableLoc = Database.loginTokensTable
    def __init__(self, guid, loginToken = None):
        self.guid = guid
        if loginToken is None:
            self.token = newToken()
            self.created = datetime.now()
        else:
            self.token = loginToken['token']
            self.created = loginToken['created']
        if guid is not None and Backend.select(self.tableLoc, self.token) is None:
            Backend.insert(self.tableLoc, self.token,
                { 'guid' : self.guid
                , 'created' : self.created
                },
            )
    def asDict(self):
        return \
            { 'guid' : self.guid
            , 'created' : self.created
            , 'token' : self.token
            }

def loginTokenNotExpired(loginToken):
    return \
        loginToken.created >= datetime.now() - timedelta(days = 1)
