from foo.pythonservice.handlers.backend import Database
from datetime import datetime

import foo.pythonservice.handlers.backend as Backend

class Resource:
    tableLoc = Database.resourcesTable
    def __init__(self, guid, res = None):
        self.guid = guid
        if res is not None:
            self.name = res['name']
            self.code = res['code']
            self.url = res['url']
            self.gslides = res['gslides']
            self.created = res['created']
    def setupAsNew(self, name, code, url, gslides):
        Backend.insert(self.tableLoc, self.guid,
            { 'code' : code
            , 'name' : name
            , 'url' : url
            , 'gslides' : gslides
            , 'created' : datetime.now()
            },
        )
        return self.guid
    def delete(self):
        Backend.delete(self.tableLoc, self.guid)
        return True

    def asDict(self):
        return \
            { 'code' : self.code
            , 'name' : self.name
            , 'url' : self.url
            , 'gslides' : self.gslides
            , 'created' : self.created
            }

class Resources:
    def __init__(self, res = { }):
        self.permitted = res
    def asDict(self):
        resources = { }
        for guid, res in self.permitted.items():
            if resourceByGuid(guid) is not None:
                try:
                    resources[guid] = res.asDict()
                except AttributeError:
                    resources[guid] = res
        return resources
    def asKeysList(self):
        return list(self.asDict().keys())
    def withPermittedURLs(self):
        urls = { }
        for res in self.permitted.values():
            urls[res.code] = res.url
        return urls
    def permittedCodesList(self):
        codes = []
        for res in self.permitted.values():
            codes.append(res['code'])
        return codes
    def permittedGuidsList(self):
        guids = []
        for guid in self.permitted.keys():
            guids.append(guid)
        return guids
    def asPermissionsDict(self):
        perms = { }
        for res in self.permitted.values():
            perms[res['code']] = True
        return perms

def allres():
    resources = { }
    for guid, res in dict(Backend.selectAll(Database.resourcesTable)).items():
        resources[guid] = Resource(guid, res)
    return Resources(res = resources)

def nores():
    return Resources()

# todo isSuccess
def registerResource(guid, name, code, url, gslides):
    return Resource(guid).setupAsNew(name, code, url, gslides)

def deleteResource(guid):
    return Resource(guid).delete()

def resourceByGuid(guid):
    return Backend.select(Database.resourcesTable, guid)

def asPermittedCodesStr(res):
    return ", ".join(res.permittedCodesList())

def resourcesFromGuidPerms(perms):
    res = { }
    for guid, hasPerms in perms.items():
        if hasPerms:
            res[guid] = resourceByGuid(guid)
    return Resources(res)
