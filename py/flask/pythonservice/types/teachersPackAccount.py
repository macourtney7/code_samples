from foo.pythonservice.handlers.backend import Database
from datetime import datetime

import foo.pythonservice.handlers.backend as Backend

class TeachersPackAccount:
    tableLoc = Database.teachersAccountsTable
    def __init__(self, guid, account = None):
        self.guid = guid
        if account is not None:
            self.username = account['username']
            self.expected = account['expected']
            self.resources = account['resources']
            self.created = account['created']
            self.last_accessed = self.setLastAccessed(account)
            self.isOTP = account['is_otp']
    def setupAsNew(self, username, expected, res):
        now = datetime.now()
        Backend.insert(self.tableLoc, self.guid,
            { 'username' : username
            , 'expected' : expected
            , 'resources' : res
            , 'created' : now
            , 'last_accessed' : now
            , 'is_otp': True
            },
        )
        return self.guid
    def delete(self):
        Backend.delete(self.tableLoc, self.guid)
        return True

    def setLastAccessed(self, account):
        account['last_accessed'] = datetime.now()
        self.last_accessed = account['last_accessed']
        return self.last_accessed

# todo: add isSuccess
def registerTeachersPackAccount(guid, username, expected, resources):
    return TeachersPackAccount(guid).setupAsNew(username, expected, resources)

def deleteAccount(guid):
    return TeachersPackAccount(guid).delete()
