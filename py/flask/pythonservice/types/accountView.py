from foo.pythonservice.handlers.backend import Database

import foo.pythonservice.handlers.accounts as Acc
import foo.pythonservice.handlers.backend as Backend
import foo.pythonservice.handlers.resources as ResHandl
import foo.pythonservice.types.resources as ResTypes

class AdminAccountMeta:
    def __init__(self, account):
        self.guid = account.guid
        self.username = account.username
        self.resources = ResTypes.asPermittedCodesStr(account.resources)
        self.created = account.created
        self.last_accessed = account.last_accessed
    def asDict(self):
        return \
            { 'guid' : self.guid
            , 'username' : self.username
            , 'resources' : self.resources
            , 'created' : self.created
            , 'last_accessed' : self.last_accessed
            }

class AdminResourceMeta:
    def __init__(self, resource):
        self.guid = resource.guid
        self.name = resource.name
        self.code = resource.code
        self.created = resource.created
    def asDict(self):
        return \
            { 'guid' : self.guid
            , 'name' : self.name
            , 'code' : self.code
            , 'created' : self.created
            }

def adminMeta():
    accountsMeta = \
        [ AdminAccountMeta(Acc.accountByGuid(guid).account).asDict()
            for guid in list(Backend.selectAll(Database.teachersAccountsTable).keys())
        ]
    resourcesMeta = \
        [ AdminResourceMeta(ResHandl.resourceByGuid(guid)).asDict()
            for guid in list(Backend.selectAll(Database.resourcesTable).keys())
        ]
    return \
        { 'accountsMeta' : accountsMeta
        , 'resourcesMeta' : resourcesMeta
        }

class AccountView:
    def __init__(self, guid, username, isAdmin = False, res = ResTypes.nores()):
        self.guid = guid
        self.username = username
        self.isAdmin = isAdmin
        self.resources = res
    def asDict(self):
        return \
            { 'guid' : self.guid
            , 'username' : self.username
            , 'isAdmin' : self.isAdmin
            , 'resources' : self.resources.asDict()
            , 'adminView': adminMeta() if self.isAdmin else []
            }
