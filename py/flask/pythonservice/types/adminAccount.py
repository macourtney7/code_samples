from foo.pythonservice.handlers.backend import Database
from datetime import datetime

import foo.pythonservice.handlers.backend as Backend

class AdminAccount:
    tableLoc = Database.adminAccountsTable
    def __init__(self, guid, account = None):
        self.guid = guid
        if account is not None:
            self.username = account['username']
            self.expected = account['expected']
            self.created = account['created']
            self.last_accessed = self.setLastAccessed(account)
    def setupAsNew(self, username, expected):
        now = datetime.now()
        Backend.insert(self.tableLoc, self.guid,
            { 'username' : username
            , 'expected' : expected
            , 'created' : now
            , 'last_accessed' : now
            },
        )
        return self.guid
    def setLastAccessed(self, account):
        account['last_accessed'] = datetime.now()
        self.last_accessed = account['last_accessed']
        return self.last_accessed

# todo: add isSuccess
def registerAdminAccount(guid, username, expected):
    return AdminAccount(guid).setupAsNew(username, expected)
