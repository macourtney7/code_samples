import pickle

from foo.config.config import Config
from sqlalchemy import create_engine, Column, String, PickleType, insert, update, delete
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

dbEngine = create_engine(
        Config.database.loc,
        pool_recycle = 280,
    )

Base = declarative_base()
class AdminAccountsTable(Base):
    __tablename__ = Config.database.adminAccountsTable
    guid = Column(String(64), primary_key = True)
    v = Column(PickleType)
    def __repr__(self):
        return f"AdminAccountsTable(guid = {self.guid!r}, v = {self.v!r})"

class TeachersAccountsTable(Base):
    __tablename__ = Config.database.teachersAccountsTable
    guid = Column(String(64), primary_key = True)
    v = Column(PickleType)
    def __repr__(self):
        return f"TeachersAccountsTable(guid = {self.guid!r}, v = {self.v!r}"

class LoginTokensTable(Base):
    __tablename__ = Config.database.loginTokensTable
    guid = Column(String(64), primary_key = True)
    v = Column(PickleType)
    def __repr__(self):
        return f"LoginTokensTable(guid = {self.guid!r}, v = {self.v!r}"

class ResourcesTable(Base):
    __tablename__ = Config.database.resourcesTable
    guid = Column(String(64), primary_key = True)
    v = Column(PickleType)
    def __repr__(self):
        return f"ResourcesTable(guid = {self.guid!r}, v = {self.v!r}"
Base.metadata.create_all(dbEngine)

Session = sessionmaker(bind = dbEngine)

class Database:
    adminAccountsTable = AdminAccountsTable
    teachersAccountsTable = TeachersAccountsTable
    loginTokensTable = LoginTokensTable
    resourcesTable = ResourcesTable

def insert(table, guid, value):
    with Session() as session:
        session.add(table(guid = guid, v = pickle.dumps(value)))
        session.commit()

def update(table, guid, value):
    with Session() as session:
        session.query(table).filter_by(guid = guid).first().v = pickle.dumps(value)
        session.commit()

def delete(table, guid):
    with Session() as session:
        session.query(table).filter_by(guid = guid).delete()
        session.commit()

def select(table, guid):
    try:
        return selectAll(table)[guid]
    except KeyError:
        return None

def selectAll(table):
    rels = { }
    with Session() as session:
        for i in session.query(table).all():
            rels[i.guid] = pickle.loads(i.v)
    return rels
