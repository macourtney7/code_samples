from foo.pythonservice.handlers.backend import Database
from foo.pythonservice.types.ids import LoginToken, loginTokenNotExpired
from datetime import datetime
from flask import json

import foo.pythonservice.handlers.accounts as Acc
import foo.pythonservice.handlers.backend as Backend
import foo.pythonservice.types.accountView as AV
import foo.pythonservice.types.activeUser as AU

def validateExpected(guid, isAdmin, expected):
    if isAdmin:
        return expected == Backend.select(Database.adminAccountsTable, guid)['expected']
    return expected == Backend.select(Database.teachersAccountsTable, guid)['expected']

def attemptLogin(username, expected):
    ((guid, account), isAdmin) = Acc.accountByUsername(username)
    if guid is not None and validateExpected(guid, isAdmin, expected):
        return AU.ActiveUser(guid, account, isAdmin)
    return (None)

def loginResponse(session, activeUser):
    resp = { 'success' : False }
    if activeUser is not None:
        resp = \
            { 'success' : True
            , 'guid' : AU.guidIfNotOTP(activeUser)
            , 'username' : activeUser.username()
            , 'isOTP' : activeUser.isOTP()
            }
        session['loginToken'] = json.dumps(activeUser.loginToken.asDict())
        session.modified = True
    return (json.dumps(resp), 200)

def tokenCheck(loginToken):
    if loginToken is not None and loginTokenNotExpired(loginToken):
        account = Acc.accountByLoginToken(loginToken)
        if account is not None:
            return AV.AccountView(
                account.guid,
                account.username(),
                isAdmin = account.isAdmin,
                res = account.resources(),
            )
    return None

def tokenIsAdmin(loginToken):
    return Acc.accountByLoginToken(loginToken).isAdmin # seems dodgy

def accountViewData(accountView):
    resp = { 'success' : False }
    if accountView is not None:
        resp = \
            { 'success' : True
            , 'accountView' : accountView.asDict()
            }
    return json.dumps(resp)

def sessionWithLoginToken(session):
    if 'loginToken' not in session:
        session['loginToken'] = None
    return session

def loginTokenFromSession(guid, session):
    token = sessionWithLoginToken(session)['loginToken']
    if token is not None:
        token = json.loads(token)
        token['created'] = datetime.strptime(token['created'], '%a, %d %b %Y %H:%M:%S %Z')
        if token['guid'] == guid:
            return LoginToken(
                guid,
                loginToken = token,
            )
        return LoginToken(guid)
    return None

def guidFromSession(session):
    loginToken = sessionWithLoginToken(session)['loginToken']
    if loginToken is not None:
        return Backend.select(Database.loginTokensTable, json.loads(loginToken)['token'])['guid']
    return None

def guidFromSessionIfNotOTP(session):
    guid = guidFromSession(session)
    if guid is not None:
        account = Acc.accountByGuid(guid)
        return (None if (account is None or account.isOTP()) else guid)
    return None
