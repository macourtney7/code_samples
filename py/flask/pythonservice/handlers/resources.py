from foo.pythonservice.handlers.backend import Database
from foo.pythonservice.types.ids import newGuid
from flask import json

import foo.pythonservice.handlers.backend as Backend
import foo.pythonservice.types.resources as Res

def attemptRegisterResource(name, code, url, gslides):
    if isResourceNameCodeAvailable(name, code):
        return Res.registerResource(
            newGuid(),
            name,
            code,
            url,
            gslides,
        )
    return None

def registerResourceResponse(guid):
    resource = Res.resourceByGuid(guid)
    if resource is not None:
        return \
            ( json.dumps(
                { 'success' : True
                , 'newResource' : \
                    { 'guid' : guid
                    , 'name' : resource['name']
                    , 'code' : resource['code']
                    , 'created' : resource['created']
                    }
                }
              )
            , 200
            )
    return \
        ( json.dumps(
            { 'success' : False
            }
          )
        , 200
        )

def resourceByGuid(guid):
    res = Backend.select(Database.resourcesTable, guid)
    if res is not None:
        return Res.Resource(
            guid,
            res,
        )
    return None

def resourceByName(name, ignoreCase = False):
    resources = Backend.selectAll(Database.resourcesTable)
    maybeUpper = lambda name, ignore: name.upper() if ignore else name
    casedName = maybeUpper(name, ignoreCase)
    for guid in list(dict(resources).keys()):
        if casedName == maybeUpper(resources[guid]['name'], ignoreCase):
            return resources[guid]
    return None

def resourceByCode(code):
    resources = Backend.selectAll(Database.resourcesTable)
    for guid in list(dict(resources).keys()):
        if code == resources[guid]['code']:
            return resources[guid]
    return None

def attemptDeleteResource(guid):
    return Res.deleteResource(
        guid,
    )

def deleteResourceResponse(attempt):
    return \
        ( json.dumps(
            { 'success' : attempt
            }
          )
        , 200
        )

def isResourceNameCodeAvailable(name, code):
    return resourceByName(name, ignoreCase = True) is None and \
        resourceByCode(code) is None
