def attemptLogout(session):
    if 'loginToken' in session:
        session.pop('loginToken', None)
    return True