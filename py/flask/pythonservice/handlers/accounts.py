import hashlib

from foo.pythonservice.handlers.backend import Database
from foo.pythonservice.types.ids import newGuid

import foo.pythonservice.handlers.backend as Backend
import foo.pythonservice.types.activeUser as AU
import foo.pythonservice.types.adminAccount as AA
import foo.pythonservice.types.resources as Res
import foo.pythonservice.types.teachersPackAccount as TPA

def accountByAdminGuid(guid):
    return accountInTableByGuid(
        guid,
        Backend.select(Database.adminAccountsTable, guid),
        isAdmin = True,
    )

def accountByUserGuid(guid):
    return accountInTableByGuid(
        guid,
        Backend.select(Database.teachersAccountsTable, guid),
    )

def accountInTableByGuid(guid, account, isAdmin = False):
    if account is not None:
        return AU.ActiveUser(
            guid,
            account,
            isAdmin,
        )
    return None

def accountByGuid(guid):
    account = accountByUserGuid(guid)
    if account is not None:
        return account
    return accountByAdminGuid(guid)

def accountIsUser(guid):
    return Backend.select(Database.teachersAccountsTable, guid) is not None

def accountIsAdmin(guid):
    return Backend.select(Database.adminAccountsTable, guid) is not None

def accountByAdminUsername(username):
    return accountInTableByUsername(
        Backend.selectAll(Database.adminAccountsTable),
        username,
    )

def accountByUserUsername(username):
    return accountInTableByUsername(
        Backend.selectAll(Database.teachersAccountsTable),
        username,
    )

def accountInTableByUsername(accounts, username):
    for guid in list(dict(accounts).keys()):
        if username == accounts[guid]['username']:
            return (guid, accounts[guid])
    return (None, None)

def isUsernameAvailable(username):
    return accountByAdminUsername(username) == (None, None) and \
        accountByUserUsername(username) == (None, None)

def accountByUsername(username):
    user = accountByUserUsername(username)
    admin = accountByAdminUsername(username)
    if user != (None, None):
        return (user, False)
    elif admin != (None, None):
        return (admin, True)
    return ((None, None), False)

def registerAccount(username, password, isAdmin = False, resources = Res.nores()):
    if isAdmin:
        return AA.registerAdminAccount(
            newGuid(),
            username,
            hashlib.sha224(bytes(password, encoding = 'utf8')).hexdigest(),
        )
    else:
        return TPA.registerTeachersPackAccount(
            newGuid(),
            username,
            hashlib.sha224(bytes(password, encoding = 'utf8')).hexdigest(),
            resources,
        )

class AccountUpdate:
    tableLoc = Database.teachersAccountsTable
    def __init__(self, guid, username = None, expected = None, resources = None, created = None, lastAccessed = None, isOTP = None, isAdmin = False):
        self.guid = guid
        self.isAdmin = isAdmin
        if self.isAdmin:
            self.tableLoc = Database.adminAccountsTable
        account = Backend.select(self.tableLoc, guid)
        if username is not None:
            self.username = username
        else:
            self.username = account['username']
        if expected is not None:
            self.expected = expected
        else:
            self.expected = account['expected']
        if resources is not None and not self.isAdmin:
            self.resources = resources
        elif not self.isAdmin:
            self.resources = account['resources']
        if created is not None:
            self.created = created
        else:
            self.created = account['created']
        if lastAccessed is not None:
            self.lastAccessed = lastAccessed
        else:
            self.lastAccessed = account['last_accessed']
        if isOTP is not None and not self.isAdmin:
            self.isOTP = isOTP
        elif not self.isAdmin:
            self.isOTP = account['is_otp']
    def attemptUpdate(self):
        update = \
            { 'username' : self.username
            , 'expected' : self.expected
            , 'created' : self.created
            , 'last_accessed' : self.lastAccessed
            }
        if not self.isAdmin:
            update['resources'] = self.resources
            update['is_otp'] = self.isOTP
        Backend.update(self.tableLoc, self.guid, update)

def accountByLoginToken(loginToken):
    try:
        return accountByGuid(
            Backend.select(Database.loginTokensTable, loginToken.token)['guid']
        )
    except KeyError:
        return None
