from foo.config.config import Config
from foo.pythonservice.handlers.backend import Database
from foo.pythonservice.types.ids import newToken
from flask import json

import foo.pythonservice.handlers.accounts as Acc
import foo.pythonservice.handlers.backend as Backend
import foo.pythonservice.types.resources as Res
import foo.pythonservice.types.teachersPackAccount as TPA

def attemptRegisterAccount(username, perms):
    otp = newToken()
    res = Res.resourcesFromGuidPerms(perms)
    if Acc.isUsernameAvailable(username):
        return \
            ( Acc.registerAccount(
                username,
                otp,
                resources = res,
              )
            , otp
            )
    return (None, None)

def registerAccountResponse(attempt):
    (guid, otp) = attempt
    if guid is not None:
        account = Acc.accountByUserGuid(guid)
        if account is not None:
            return \
                ( json.dumps(
                    { 'success' : True
                    , 'newAccount' : \
                        { 'guid' : account.guid
                        , 'username' : account.username()
                        , 'resources' : Res.asPermittedCodesStr(account.resources())
                        , 'created' : account.created()
                        , 'last_accessed' : account.last_accessed()
                        , 'otp' : otp
                        }
                    }
                  )
                , 200
                )
    return \
        ( json.dumps(
            { 'success' : False
            }
          )
        , 200
        )

def attemptPasswordChange(username, prevExpected, newExpected):
    ((guid, _), _) = Acc.accountByUsername(username)
    if Acc.accountIsUser(guid) and \
        prevExpected == Backend.select(Database.teachersAccountsTable, guid)['expected']:
            Acc.AccountUpdate(
                guid,
                expected = newExpected,
                isOTP = False,
            ).attemptUpdate()
            return (username, True)
    elif Acc.accountIsAdmin(guid) and \
        prevExpected == Backend.select(Database.adminAccountsTable, guid)['expected']:
            Acc.AccountUpdate(
                guid,
                expected = newExpected,
                isAdmin = True,
            ).attemptUpdate()
            return (username, True)
    return (username, False)

def passwordChangeResponse(attempt):
    (username, isSuccess) = attempt
    return (json.dumps(
        { 'success' : isSuccess
        , 'username' : username
        }
    ), 200)

def attemptDeleteAccount(guid):
    return TPA.deleteAccount(
        guid,
    )

def deleteAccountResponse(attempt):
    return \
        ( json.dumps(
            { 'success' : attempt
            }
          )
        , 200
        )

def setup():
    if Acc.accountByAdminUsername(Config.adminAccount.user) == (None, None):
        Acc.registerAccount(
            Config.adminAccount.user,
            Config.adminAccount.password,
            isAdmin = True,
        )
