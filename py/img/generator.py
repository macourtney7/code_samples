"""img generator"""
import os,sys,cv2,numpy,db_cnx,subprocess,time,random,shutil

class Status:
    """generator status reporter"""
    def __init__(self):
        self.start_timestamp=int(round(time.time()*1000))
        self.uid=None

        with open("/foo/bar/log","w") as log:
            log.write("#offline_gen.py log\n")
        self.log("Status:__init__()")

    def update(self,task_msg,percent):
        current_timestamp=int(round(time.time()*1000))
        msg=task_msg+","+str(percent)+","+str(self.start_timestamp)+","+str(self.uid)
        with open("/foo/bar/status",'w') as status:
            status.write(msg)
        self.log(msg)
        subprocess.run("scp -q -i /foo/bar/.ssh/id_rsa /foo/bar/status foo@bar:~/foo/bar/status &",shell=True)

    def log(self,msg):
        datetime=time.ctime().split(" ")
        datetime=datetime[2]+"/"+datetime[1]+"/"+datetime[4]+"_["+datetime[3]+"]: "
        with open("/foo/bar/log","a") as log:
            log.write(datetime+msg+"\n")
        subprocess.run("scp -q -i /foo/bar/.ssh/id_rsa /foo/bar/log foo@bar:~/foo/bar/log &",shell=True)


class SRC:
    """src img"""
    def __init__(self,status,target_file):
        status.log("SRC:__init__()")
        self.img=db_cnx.IMG(target_file)
        os.rename(target_file,self.img.path)
        status.uid=self.img.filename
        status.update("processing src img",3) #update gui status msg
        self.__sync_db(self.img)
        status.update("processing src img",5) #update gui status msg
        self.colourgrid=ColourGrid(status,self.img)

    def __sync_db(self,img):
        db=db_cnx.DBConnect()
        if db:
            cursor=db.cursor()
            cursor.execute("CREATE TABLE "+str(img.uhid)+"(query;")
            db.close()


class ColourGrid:
    """web.site src img colourgrid"""
    def __init__(self,status,img):
        status.log("ColourGrid:__init__()")
        db=db_cnx.DBConnect()
        if db:
            self.__cursor=db.cursor()
            self.__colourvals(status,img)
            db.close()

    def __colourvals(self,status,img):
        overlay=cv2.cvtColor(cv2.imread("/foo/bar/template_6000x6000.png",cv2.IMREAD_UNCHANGED),cv2.COLOR_RGB2RGBA)
        src_imread=cv2.imread(img.path,cv2.IMREAD_UNCHANGED)
        src_height,src_width=src_imread.shape[:2]
        seg_height,seg_width=(120,120)

        i=0
        for y in range(0,src_height,seg_height):
            for x in range(0,src_width,seg_width):
                bgr=numpy.uint8(numpy.average(numpy.average(src_imread[y:y+seg_height,x:x+seg_width],axis=0),axis=0)).tolist()
                overlay[y:y+seg_height,x:x+seg_width][:,:,0]=round(bgr[0])
                overlay[y:y+seg_height,x:x+seg_width][:,:,1]=round(bgr[1])
                overlay[y:y+seg_height,x:x+seg_width][:,:,2]=round(bgr[2])
                overlay[y:y+seg_height,x:x+seg_width][:,:,3]=128
                rgb=bgr[::-1]
                self.__sync_db(img.uhid,x,y,rgb,self.__lab(rgb))

                i+=1
                if i%500==0:
                    status.update("processing src img",5+2*(i/500)) #update gui status msg
        cv2.imwrite("/foo/bar/overlays/"+img.uhid+".png",overlay)

    def __lab(self,rgb):
        for i in range(0,3):
            rgb[i]=rgb[i]/255
            if rgb[i]>0.04045:
                rgb[i]=((rgb[i]+0.055)/1.055)**2.4
            else:
                rgb[i]=rgb[i]/12.92
            rgb[i]=rgb[i]*100
        xyz=[((rgb[0]*0.4124)+(rgb[1]*0.3576)+(rgb[2]*0.1805)),((rgb[0]*0.2126)+(rgb[1]*0.7152)
            +(rgb[2]*0.0722)),((rgb[0]*0.0193)+(rgb[1]*0.1192)+(rgb[2]*0.9505))]

        for i in range(0,3):
            xyz[i]=xyz[i]/100
            if xyz[i]>0.008856:
                xyz[i]=xyz[i]**(1/3)
            else:
                xyz[i]=(7.787*xyz[i])+(16/116)
        return [(round(116*xyz[1])-16),round(500*(xyz[0]-xyz[1])),round(200*(xyz[1]-xyz[2]))]

    def __sync_db(self,uhid,x,y,rgb,lab):
        self.__cursor.execute("INSERT INTO "+str(uhid)+"(attrib) VALUES("+str(x)+","+str(y)+","
            +str(round(rgb[0]))+","+str(round(rgb[1]))+","+str(round(rgb[2]))+","+str(lab[0])+","+str(lab[1])+","+str(lab[2])+");")


class PIPS:
    """usr_img"""
    def __init__(self,status,src_img):
        status.log("PIPS:__init__()")
        db=db_cnx.DBConnect()
        if db:
            status.update("processing pips",15) #update gui status msg
            self.__cursor=db.cursor()
            self.dir=os.fsencode("/foo/bar/"+src_img.filename)
            i=0
            dir_list=os.listdir(self.dir)
            for f in dir_list:
                f=os.fsdecode(self.dir)+"/"+os.fsdecode(f)
                while True:
                    pip_img=db_cnx.IMG(f)
                    os.rename(f,pip_img.path)
                    try:
                        pip_img.avg_colour()
                        pip_img.colour_classify()
                        self.__sync_db(src_img.uhid,pip_img)
                        i+=1
                        if i%500==0:
                            status.update("processing pips",15+15*(i/500)) #update gui status msg
                        break
                    except AttributeError:
                        status.log('exception: AttributeError '+f)
                        rand_f=os.listdir(self.dir)[random.randint(0,len(os.listdir(self.dir)))]
                        shutil.copyfile(os.fsdecode(self.dir)+os.fsdecode(rand_f),f)
            db.close()

    def __sync_db(self,src_uhid,pip):
        self.__cursor.execute("UPDATE "
            +str(src_uhid)+" SET colour_tag=\""+str(pip.colour_tag)+"\",pip_uhid=\""
            +str(pip.uhid)+"\" WHERE pip_uhid='null' ORDER BY (cie_l BETWEEN "+str(round(pip.lab[0]-33))+" AND "
            +str(round(pip.lab[0]+33))+") DESC, (cie_a BETWEEN "+str(round(pip.lab[1]-33))+" AND "+str(round(pip.lab[1]+33))
            +") DESC, (cie_b BETWEEN "+str(round(pip.lab[2]-33))+" AND "+str(round(pip.lab[2]+33))+") DESC LIMIT 1;")


class IMGRenderer:
    """piptur.com img render"""
    def __init__(self,status,src_img):
        status.log("IMGRenderer:__init__()")
        self.pips=self.__retrieve_pips(src_img.uhid)
        self.__render(status,src_img.uhid,src_img.filename)

    def __retrieve_pips(self,src_uhid):
        o=[]
        db=db_cnx.DBConnect()
        if db:
            cursor=db.cursor()
            cursor.execute("SELECT x,y,pip_uhid FROM "+src_uhid+" ORDER BY x,y ASC;")
            for pip in cursor:
                o.append(pip)
        return o

    def __render(self,status,src_uhid,src_fname):
        render=cv2.imread("/foo/bar/"+src_uhid+".png",cv2.IMREAD_UNCHANGED)
        pip_layer=cv2.imread("/foo/bar/6000px_bg-layer.png",cv2.IMREAD_UNCHANGED)
        overlay=cv2.imread("/foo/bar/"+src_uhid+".png",cv2.IMREAD_UNCHANGED)
        i=0
        for y in range(0,50):
            for x in range(0,50):
                pip_layer[x*120:(x*120+120),y*120:(y*120+120)]=cv2.resize(cv2.imread("/foo/bar/"
                    +src_fname+"/"+self.pips[i][2]+".png",cv2.IMREAD_COLOR),(120,120))
                i+=1
                if i%500==0:
                    status.update("rendering img",90+2*(i/500)) #update gui status msg

        pip_layer=cv2.cvtColor(pip_layer,cv2.COLOR_RGB2RGBA)
        render=cv2.addWeighted(render,1.0,pip_layer,0.2,0)
        render=cv2.addWeighted(render,1.0,overlay,0.2,0)
        cv2.imwrite("/foo/bar/"+src_uhid+".png",render)


"""runtime script"""
status=Status()
src_img=SRC(status,sys.argv[1])
pips=PIPS(status,src_img.img)
render=IMGRenderer(status,src_img.img)
subprocess.run("scp -q -i /foo/bar/.ssh/id_rsa /foo/bar/"+str(src_img.img.uhid)
    +".png foo@bar:~/foo/bar/"+str(src_img.img.uhid)+".png &",shell=True)
status.update(str(src_img.img.uhid),100) #update gui status msg
