import sys,cv2,db_cnx

class IMGRenderer:
    """img render"""
    def __init__(self,src_uhid,src_fname):
        self.pips=self.__retrieve_pips(src_uhid)
        self.__render(src_uhid,src_fname)

    #retrieve metadata
    def __retrieve_pips(self,src_uhid):
        o=[]
        db=db_cnx.DBConnect()
        if db:
            cursor=db.cursor()
            cursor.execute("SELECT x,y,pip_uhid FROM "+src_uhid+" ORDER BY x,y ASC;")
            for pip in cursor:
                o.append(pip)
        db.close()
        return o

    #construct output file
    def __render(self,src_uhid,src_fname):
        render=cv2.imread("/foo/bar/"+src_uhid+".png",cv2.IMREAD_UNCHANGED)
        pip_layer=cv2.imread("/foo/bar/6000px_bg-layer.png",cv2.IMREAD_UNCHANGED)
        overlay=cv2.imread("/foo/bar/"+src_uhid+".png",cv2.IMREAD_UNCHANGED)
        i=0
        for y in range(0,50):
            for x in range(0,50):
                print(self.pips[i][2])
                pip_layer[x*120:(x*120+120),y*120:(y*120+120)]=cv2.resize(cv2.imread("/foo/bar/"
                    +src_fname+"/"+self.pips[i][2]+".png",cv2.IMREAD_COLOR),(120,120))
                i+=1

        cv2.imwrite("/foo/bar/temp/render.png",render)
        cv2.imwrite("/foo/bar/temp/pip_layer.png",pip_layer)
        cv2.imwrite("/foo/bar/temp/overlay.png",overlay)

        render=cv2.cvtColor(render,cv2.COLOR_RGB2RGBA)
        pip_layer=cv2.cvtColor(pip_layer,cv2.COLOR_RGB2RGBA)
        overlay=cv2.cvtColor(overlay,cv2.COLOR_RGB2RGBA)

        render=cv2.addWeighted(render,1.0,pip_layer,0.2,0)
        render=cv2.addWeighted(render,1.0,overlay,0.2,0)
        cv2.imwrite("/foo/bar/"+src_uhid+".png",render)

"""runtime script"""
IMGRenderer(sys.argv[1],sys.argv[2])
