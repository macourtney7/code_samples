import hashlib,datetime,cv2,numpy,math
from datetime import datetime

class IMG:
    """img file manager"""
    def __init__(self,path):
        self.__filename_substr=self.__substr(path)
        self.filename=path[::-1][self.__filename_substr['ext']:self.__filename_substr['dir']][::-1]
        self.extension=path[::-1][:self.__filename_substr['ext']][::-1]
        self.uhid=self.__mkuhid()
        self.path=self.__mkpath(path)
        self.rgb=0
        self.lab=0
        self.pip=0
        self.piphref=0
        self.greyscale=0
        self.colour_tag=""

    #filename metadata
    def __substr(self,path):
        substr={'ext':0,'dir':0}
        for char in path[::-1]:
            if char=='.':
                substr['dir']+=1
                substr['ext']=substr['dir']
            elif char=='/':
                break
            else:
                substr['dir']+=1
        return substr

    #gen unique id
    def __mkuhid(self):
        return hashlib.md5( (self.filename+"["+str(datetime.now())[0:10]+"_"+str(datetime.now())[11:19]+"]").encode("utf-8") ).hexdigest()

    #update path
    def __mkpath(self,path):
        return path[::-1][self.__filename_substr['dir']:][::-1]+str(self.uhid)+path[::-1][:self.__filename_substr['ext']][::-1]

    #compute average colour
    def avg_colour(self):
        img=cv2.imread(self.path)
        img_height,img_width=img.shape[:2]
        self.rgb=(numpy.uint8(numpy.average(numpy.average(img[:img_height,:img_width],axis=0),axis=0)).tolist())[::-1]
        return self.rgb

    #gen mini img
    def mkpip(self,href):
        self.piphref=href
        self.px_120=cv2.imread(self.path)
        self.px_12=cv2.imread(self.path)
        self.pip="/foo/bar/"
        cv2.imwrite(self.pip+"foo/"+str(self.uhid)+str(self.extension),(cv2.resize(self.px_120,(120,120),cv2.INTER_AREA)))
        cv2.imwrite(self.pip+"foo/"+str(self.uhid)+str(self.extension),(cv2.resize(self.px_12,(12,12),cv2.INTER_AREA)))

    #gen greyscale version
    def mkgreyscale(self):
        img=cv2.imread(self.path,cv2.IMREAD_GRAYSCALE)
        self.greyscale="/foo/bar/"+str(self.uhid)+str(self.extension)
        self.greyscale_600px="/foo/bar/"+str(self.uhid)+str(self.extension)
        cv2.imwrite(self.greyscale,img)
        cv2.imwrite(self.greyscale_600px,cv2.resize(img,(600,600)))
        return self.greyscale

    #tag colour
    def colour_classify(self):
        rgbcomp=RGBComp()
        self.colour_tag=rgbcomp.classify(self.path)
        self.lab=rgbcomp.lab_o
        return self.colour_tag
"""
img=IMG("path/to/the/file.png")
print(img.filename())
print(img.uhid())
print(img.path())
"""

class RGBComp:
    """rgb compare tools"""
    def __init__(self):
        #comparison patches
        self.patches={
            'blue':{
                '001':"/foo/bar/res/patches/blue/001_blue.png",
                '002':"/foo/bar/res/patches/blue/002_blue.png",
                '003':"/foo/bar/res/patches/blue/003_blue.png",
                '004':"/foo/bar/res/patches/blue/004_blue.png",
                '005':"/foo/bar/res/patches/blue/005_blue.png",
                '006':"/foo/bar/res/patches/blue/006_blue.png",
            },
            'green':{
                '001':"/foo/bar/res/patches/green/001_green.png",
                '002':"/foo/bar/res/patches/green/002_green.png",
                '003':"/foo/bar/res/patches/green/003_green.png",
                '004':"/foo/bar/res/patches/green/004_green.png",
                '005':"/foo/bar/res/patches/green/005_green.png",
                '006':"/foo/bar/res/patches/green/006_green.png",
                '007':"/foo/bar/res/patches/green/007_green.png"
            },
            'light_blue':{
                '001':"/foo/bar/res/patches/light-blue/001_light-blue.png",
                '002':"/foo/bar/res/patches/light-blue/002_light-blue.png",
                '003':"/foo/bar/res/patches/light-blue/003_light-blue.png",
                '004':"/foo/bar/res/patches/light-blue/004_light-blue.png",
                '005':"/foo/bar/res/patches/light-blue/005_light-blue.png",
                '006':"/foo/bar/res/patches/light-blue/006_light-blue.png",
                '007':"/foo/bar/res/patches/light-blue/007_light-blue.png"
            },
            'orange':{
                '001':"/foo/bar/res/patches/orange/001_orange.png",
                '002':"/foo/bar/res/patches/orange/002_orange.png",
                '003':"/foo/bar/res/patches/orange/003_orange.png",
                '004':"/foo/bar/res/patches/orange/004_orange.png",
                '005':"/foo/bar/res/patches/orange/005_orange.png",
                '006':"/foo/bar/res/patches/orange/006_orange.png",
                '007':"/foo/bar/res/patches/orange/007_orange.png"
            },
            'pink':{
                '001':"/foo/bar/res/patches/pink/001_pink.png",
                '002':"/foo/bar/res/patches/pink/002_pink.png",
                '003':"/foo/bar/res/patches/pink/003_pink.png",
                '004':"/foo/bar/res/patches/pink/004_pink.png",
                '005':"/foo/bar/res/patches/pink/005_pink.png",
                '006':"/foo/bar/res/patches/pink/006_pink.png",
            },
            'red':{
                '001':"/foo/bar/res/patches/red/001_red.png",
                '002':"/foo/bar/res/patches/red/002_red.png",
                '003':"/foo/bar/res/patches/red/003_red.png",
                '004':"/foo/bar/res/patches/red/004_red.png",
                '005':"/foo/bar/res/patches/red/005_red.png",
                '006':"/foo/bar/res/patches/red/006_red.png",
            },
            'yellow':{
                '001':"/foo/bar/res/patches/yellow/001_yellow.png",
                '002':"/foo/bar/res/patches/yellow/002_yellow.png",
                '003':"/foo/bar/res/patches/yellow/003_yellow.png",
                '004':"/foo/bar/res/patches/yellow/004_yellow.png",
                '005':"/foo/bar/res/patches/yellow/005_yellow.png",
                '006':"/foo/bar/res/patches/yellow/006_yellow.png",
            }
        }
        self.lab_o=0

    #manage lab classification
    def classify(self,patch):
        D=-1
        K=""
        for k in self.patches:
            min_d=-1
            for i in self.patches[k]:
                d=self.compare(self.lab(self.rgb(patch)),self.lab(self.rgb(self.patches[k][i])))
                if min_d==-1 or d<min_d:
                    min_d=d
            if D==-1 or min_d<D:
                D=min_d
                K=k
        return K

    #lab-based compare
    def compare(self,lab_a,lab_b):
        weight={'l':1,'c':1,'h':1}
        xC1=math.sqrt((lab_a[1]**2)+(lab_a[2]**2))
        xC2=math.sqrt((lab_b[1]**2)+(lab_b[2]**2))
        xDL=lab_a[0]-lab_b[0]
        xDC=xC2-xC1
        xDE=math.sqrt(((lab_a[0]-lab_b[0])**2)+((lab_a[1]-lab_b[1])**2)+((lab_a[2]-lab_b[2])**2))
        xDH=(xDE*xDE)-(xDL*xDL)-(xDC*xDC)

        if xDH>0:
            xDH=math.sqrt(xDH)
        else:
            xDH=0

        xSC=1+(0.045*xC1)
        xSH=1+(0.015*xC1)
        xDL=xDL/weight['l']
        xDC=xDC/weight['c']*xSC
        xDH=xDH/weight['h']*xSH
        return math.sqrt(xDL**2+xDC**2+xDH**2)

    #gen rgb value
    def rgb(self,patch_path):
        img=cv2.imread(patch_path)
        img_height,img_width=img.shape[:2]
        return (numpy.uint8(numpy.average(numpy.average(img[:img_height,:img_width],axis=0),axis=0)).tolist())[::-1]

    #convert rgb to lab
    def lab(self,rgb):
        for i in range(0,3):
            rgb[i]=rgb[i]/255
            if rgb[i]>0.04045:
                rgb[i]=((rgb[i]+0.055)/1.055)**2.4
            else:
                rgb[i]=rgb[i]/12.92
            rgb[i]=rgb[i]*100
        xyz=[((rgb[0]*0.4124)+(rgb[1]*0.3576)+(rgb[2]*0.1805)),((rgb[0]*0.2126)+(rgb[1]*0.7152)+(rgb[2]*0.0722)),((rgb[0]*0.0193)+(rgb[1]*0.1192)+(rgb[2]*0.9505))]

        for i in range(0,3):
            xyz[i]=xyz[i]/100
            if xyz[i]>0.008856:
                xyz[i]=xyz[i]**(1/3)
            else:
                xyz[i]=(7.787*xyz[i])+(16/116)
        self.lab_o=[((116*xyz[1])-16),(500*(xyz[0]-xyz[1])),(200*(xyz[1]-xyz[2]))]
        return self.lab_o
