import db_config,mysql.connector
from mysql.connector import errorcode

class DBConnect:
    """db connection manager"""
    def __init__(self):
        self.__db=self.__connect(db_config.DBConfig().config())
        self.__cursor=self.__db.cursor()

    def __connect(self,config):
        try:
            return mysql.connector.connect(**config)
        except mysql.connector.Error as err:
            return False

    def cursor(self,buffered=False):
        if not buffered:
            return self.__cursor
        self.__cursor=self.__db.cursor(buffered=True)
        return self.__cursor

    def commit(self):
        if self.__db:
            self.__db.commit()

    def close(self,commit=True):
        self.__cursor.close()
        if self.__db and commit:
            self.__db.commit()
        self.__db.close()
"""
db=DBConnect()
cursor=db.cursor()
db.close()
"""
