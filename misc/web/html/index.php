<!--index.php-->
<!doctype html>
<html lang="en-gb">
<meta charset="utf-8">
<meta name="author" content="author">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="an img-ai-based art-media platform - :v(&alpha;lpha)">
<link type="image/png" rel="icon" href="/foo/bar/res/img/favicon.png">
<title>web.site</title>
<?php
  require ("/foo/bar/res/php_func/index.php");
  header("http/1.1 303");
  header("location: https://$_SERVER[HTTP_HOST]/".homepage_loc());
  die;
?>
</html>
