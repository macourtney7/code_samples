/*uploader_2019-06-23.js*/
$(function(){
  (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
  var utmpid,fname,/*single_href,*/batch_files,batch_files_metadata;
  var confirm_window_timestamp=0;
  var valid_single_file=false;
  var valid_batch_file=false;
  var single_processing=false;
  var single_complete=false;
  var batch_processing=false;
  var batch_complete=false;

//hover behaviour
  $('#uploader_close_btn').mouseover(function(){ $('#uploader_close_btn').css('background-image',"url('/foo/bar/exit_hover.png')"); });
  $('#uploader_close_btn').mouseout(function(){ $('#uploader_close_btn').css('background-image',"url('/foo/bar/exit.png')"); });

//scroll behaviour
  //$('#uploader_batch_preview').on('mousewheel',{passive:true},(function(evnt){
  $('#uploader_batch_preview').mousewheel(function(evnt){
    delta=evnt.deltaFactor*evnt.deltaY;
    if(Math.abs(delta)>0) {
      evnt.preventDefault();
      $('#uploader_batch_preview').scrollLeft($('#uploader_batch_preview').scrollLeft()+delta);
    }
  });

//click behaviour
  $('.uploader_o_splash').click(function(){ $('#uploader_file').click(); });

  $('.uploader_upload_btn').click(function(){
    var tos_msg="Please confirm upload complies with terms of service:\nhttps://foobar/ToS";
  //batch_file upload
    if(!batch_complete && !batch_processing && $('#uploader_batch_preview').html().length>0 && valid_batch_file) {
      batch_processing=true;
      usr_confirm=confirm(tos_msg);
      confirm_window_timestamp=$.now();

      if(usr_confirm){
        var form_data=new FormData();
        form_data.append('batch_files_metadata',batch_files_metadata);
        $.ajax({
          url:'https://foobar/uploader_2019-05-12.php',
          dataType:'text',
          cache:false,
          contentType:false,
          processData:false,
          data:form_data,
          type:'post',
          beforeSend:function(){
            $('#uploader_close_btn_overlay').css('display','none');
            $('.uploader_confirm_btn_overlay').css('display','none');
            $('.uploader_upload_btn').css('display','none');
            $('#uploader_gif').css('display','flex');
            if(batch_files.length>3){
              $('.uploader_o').css('display','flex');
              $('.uploader_o_warning').css('display','initial');
            }
          },
          success:function(href){
            batch_processing=false;
            batch_complete=true;

            if(href!='-1'){
              $('#uploader_gif').css('display','none');
              $('.uploader_o').css('display','none');
              $('.uploader_o_warning').css('display','none');

              var o="";
              $.each(href.split(" "),function(k,url){ o=o+"<img class='uploader_batch_img' src='"+url+"'>"; });
              $('#uploader_batch_preview').html(o);
              $('#uploader_batch_preview').scrollLeft(80);
              $('.uploader_confirm_btn_overlay').css('display','flex');
              $('.uploader_done_btn').css('display','initial');
            } else {
              alert('PNG/JPG - 2MB LIMIT [PNGs please!] (75)');
              $('#uploader_close_btn').click();
            }
          },
          error:function(){
            alert('PNG/JPG - 2MB LIMIT [PNGs please!]');
            $('#uploader_close_btn').click();
          }/*,
          error:function(xhr,thrownError){ alert("error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
        });
      } else { batch_processing=false; }
    }
  //single_file upload
    else if(!single_complete && !single_processing && valid_single_file){
      single_processing=true;
      usr_confirm=confirm(tos_msg);
      confirm_window_timestamp=$.now();

      if(usr_confirm){
        var form_data=new FormData();
        form_data.append('utmpid',utmpid);
        form_data.append('fname',fname);
        if(!$.browser.mobile){ form_data.append('async','0'); }
        else { form_data.append('async','1'); }
        $.ajax({
          url:'https://foobar/uploader_2019-06-23.php',
          dataType:'text',
          cache:false,
          contentType:false,
          processData:false,
          data:form_data,
          type:'post',
          beforeSend:function(){
            if(!$.browser.mobile) {
              $('#uploader_close_btn_overlay').css('display','none');
              $('.uploader_confirm_btn_overlay').css('display','none');
              $('.uploader_upload_btn').css('display','none');
              $('#uploader_gif').css('display','flex');
            } else {
              $('#m_uploader_btn').css('display','none');
              $('#m_uploader_confirm_btn').css('display','none');
              $('#m_uploader_cancel_btn').css('display','none');
              $('#m_uploader_gif').css('display','flex');
            }
          },
          success:function(href){
              single_processing=false;
              single_complete=true;
            if(href!='-1'){
              //single_href=href.substring(0,href.length-6);
              if(!$.browser.mobile) {
                $('#uploader_gif').css('display','none');
                $('.uploader_confirm_btn_overlay').css('display','flex');
                $('.uploader_done_btn').css('display','initial');
              } else {
                $('#m_uploader_gif').css('display','none');
                $('#m_uploader_btn').css('display','flex');
                $('#m_uploader_done_btn').css('display','initial');
              }
            } else {
              alert(/*'.uploader_upload_btn.success */'PNG/JPG - 2MB LIMIT [PNGs please!]');
              if(!$.browser.mobile) { $('#uploader_close_btn').click(); }
              else { $('#m_uploader_cancel_btn').click(); }
            }
          },
          error:function(){
            alert(/*'.uploader_upload_btn.error */'PNG/JPG - 2MB LIMIT [PNGs please!]');
            if(!$.browser.mobile) { $('#uploader_close_btn').click(); }
            else { $('#m_uploader_cancel_btn').click(); }
          }/*,
          error:function(xhr,thrownError){ alert(".uploader_upload_btn error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
        });
      } else { single_processing=false; }
    }
  });

//uploader_batch_img click behaviour
  $('#uploader_batch_preview').click(function(evnt){
    evnt.preventDefault();
    if(batch_complete){
      var x_min=$('#uploader_preview').position().left;
      var x_max=x_min+$('#uploader_preview').width();
      var x_offset=$('#uploader_batch_preview').scrollLeft();

      var i=Math.floor(x_offset/239)+1;
      if(evnt.pageX>(x_max-(x_offset%239))) { i++; }

      var form_data=new FormData();
      form_data.append('img_href',$('#uploader_batch_preview img:nth-of-type('+String(i)+')').attr('src'));
      $.ajax({
        url:'https://foobar/img_viewer_session.php',
        dataType:'text',
        cache:false,
        contentType:false,
        processData:false,
        data:form_data,
        type:'post',
        success:function(status){ if(status) { window.open('https://foobar/img_viewer.php','_blank'); } }/*,
        error:function(xhr,thrownError){ alert("error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
      });
    }
  });
//hidden <input> type='file'
  $('#uploader_file').change(function(){
    if(!(typeof $('#uploader_file').prop('files')[0]==='undefined')){
      var form_data=new FormData();
      form_data.append('file',$('#uploader_file').prop('files')[0]);
      $.ajax({
        url:'https://foobar/uploader_2019-06-23.php',
        dataType:'text',
        cache:false,
        contentType:false,
        processData:false,
        data:form_data,
        type:'post',
        beforeSend:function(){
          single_processing=true;
          if(!$.browser.mobile) {
            $('#uploader_close_btn_overlay').css('display','none');
            $('.uploader_o').css('display','none');
            $('.uploader_o_splash').css('display','none');
            $('#uploader_preview').css('background-color','transparent');
            $('#uploader_preview').css('border-color','#7f7e7e');
            $('#uploader_gif').css('display','flex');
          } else {
            $('#m_uploader_container').css('display','flex');
            $('#m_uploader_gif').css('display','flex');
          }
        },
        success:function(data){
          single_processing=false;
          if(data.length>0 && data!='-1'){
            valid_single_file=true;
            valid_batch_file=false;

            var data_split=data.split("_");
            utmpid=data_split[0];
            fname=data_split[1];
            var deg=data_split[2];

            if(!$.browser.mobile) {
              $('#uploader_preview').css('background-image',"url('https://foobar/utmp/"+utmpid+".png')");
              $('#uploader_gif').css('display','none');
              $('#uploader_close_btn_overlay').css('display','flex');
              $('.uploader_confirm_btn_overlay').css('display','flex');
              $('.uploader_upload_btn').css('display','initial');
            } else {
              $('#m_uploader_gif').css('display','none');
              $('#m_uploader_container').css('display','flex');
              $('#m_uploader_preview').css('background-image',"url('https://foobar/"+utmpid+".png')");
              $('#m_uploader_preview').css('transform','rotate('+deg+'deg)');
              $('#m_uploader_btn').css('display','flex');
              $('#m_uploader_confirm_btn').css('display','initial');
              $('#m_uploader_cancel_btn').css('display','initial');
            }
          } else {
            alert(/*'#uploader_file.success*/'PNG/JPG - 2MB LIMIT [PNGs please!]');
            if(!$.browser.mobile) { $('#uploader_close_btn').click(); }
            else { $('#m_uploader_cancel_btn').click(); }
          }
        },
        error:function(){
          alert(/*'#uploader_file.error*/'PNG/JPG - 2MB LIMIT [PNGs please!]');
          if(!$.browser.mobile) { $('#uploader_close_btn').click(); }
          else { $('#m_uploader_cancel_btn').click(); }
        }/*,
        error:function(xhr,thrownError){ alert("#uploader_file error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
      });
    }
  });
//user control buttons
  $('#uploader_close_btn').click(function(evnt){
    evnt.preventDefault();

    valid_single_file=false;
    valid_batch_file=false;
    single_processing=false;
    single_complete=false;
    batch_processing=false;
    batch_complete=false;

    $('.uploader_o').css('display','flex');
    $('.uploader_o_splash').css('display','initial');
    $('#uploader_close_btn_overlay').css('display','none');
    $('.uploader_confirm_btn_overlay').css('display','none');
    $('.uploader_upload_btn').css('display','none');
    $('#uploader_preview').css('background-image','none');
    $('#uploader_preview').css('background-color','transparent');
    $('#uploader_preview').css('border-color','#7f7e7e');
    $('#uploader_batch_preview').html("");
    $('#uploader_gif').css('display','none');
  });
  $('.uploader_done_btn').click(function(evnt){
    evnt.preventDefault();
    location.reload(true);
  });
  $('#m_uploader_cancel_btn').click(function(evnt){
    evnt.preventDefault();

    valid_single_file=false;
    single_processing=false;
    single_complete=false;

    $('#m_uploader_preview').css('background-image','none');
    $('#m_uploader_container').css('display','none');
    $('#m_uploader_btn').css('display','none');
  });
  /*
   *  m_uploader_confirm_btn start asyn version of ajax
  */
  $('#m_uploader_confirm_btn').click(function(){ $('.uploader_upload_btn').click(); });
  $('#m_uploader_done_btn').click(function(){ $('.uploader_done_btn').click(); });
//dragndrop functionality
  $('#uploader_preview').on('dragover',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
  });
  $('#uploader_preview').on('dragenter',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
    if($('.uploader_o_splash').css('display')=='block'){
      $('#uploader_preview').css('background-color','rgba(3,248,14,.33)');
      $('#uploader_preview').css('border-color','black');
    }
  });
  $('#uploader_preview').on('dragleave',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
    if($('.uploader_o_splash').css('display')=='block'){
      $('#uploader_preview').css('background-color','transparent');
      $('#uploader_preview').css('border-color','#7f7e7e');
    }
  });
  //drop
  $('#uploader_preview').on('drop',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
    timestamp=$.now()-confirm_window_timestamp;
    if(timestamp>500 && !single_processing && !single_complete && !batch_processing && !batch_complete &&
      evnt.originalEvent.dataTransfer && evnt.originalEvent.dataTransfer.files.length) {
        batch_files=evnt.originalEvent.dataTransfer.files;
    //single_file
      if(batch_files.length==1) {
        var form_data=new FormData();
        form_data.append('file',batch_files[0]);
        $.ajax({
          url:'https://foobar/uploader_2019-05-12.php',
          dataType:'text',
          cache:false,
          contentType:false,
          processData:false,
          data:form_data,
          type:'post',
          beforeSend:function(){
            single_processing=true;
            $('#uploader_close_btn_overlay').css('display','none');
            $('#uploader_preview').css('background-color','transparent');
            $('#uploader_preview').css('border-color','#7f7e7e');
            $('.uploader_o').css('display','none');
            $('.uploader_o_splash').css('display','none');
            $('#uploader_gif').css('display','flex');
          },
          success:function(data){
            single_processing=false;

            if(data.length>0 && data!='-1') {
              valid_single_file=true;
              valid_batch_file=false;
              utmpid=data.substring(0,6);
              fname=data.substring(6,data.length-4);
              $('#uploader_preview').css('background-image',"url('https://foobar/"+utmpid+".png')");
              $('#uploader_batch_preview').html("");
              $('#uploader_gif').css('display','none');
              $('#uploader_close_btn_overlay').css('display','flex');
              $('.uploader_confirm_btn_overlay').css('display','flex');
              $('.uploader_upload_btn').css('display','initial');
            } else {
              alert('PNG/JPG - 2MB LIMIT [PNGs please!]');
              $('#uploader_close_btn').click();
            }
          },
          error:function(){
            alert('PNG/JPG - 2MB LIMIT [PNGs please!]');
            $('#uploader_close_btn').click();
          }/*,
          error:function(xhr,thrownError){ alert("error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
        });
    //batch_file
      } else {
        var form_data=new FormData();
        for(var i=0; (i<5 && i<batch_files.length); i++) { form_data.append(i,batch_files[i]); }
        $.ajax({
          url:'https://foobar/uploader_2019-05-12.php',
          dataType:'text',
          cache:false,
          contentType:false,
          processData:false,
          data:form_data,
          type:'post',
          beforeSend:function(){
            batch_processing=true;
            $('#uploader_close_btn_overlay').css('display','none');
            $('#uploader_preview').css('background-color','transparent');
            $('#uploader_preview').css('border-color','#7f7e7e');
            $('.uploader_o').css('display','none');
            $('.uploader_o_splash').css('display','none');
            $('#uploader_gif').css('display','flex');
          },
          success:function(data){
            batch_processing=false;

            if(data.length>0 && data!='-1') {
              valid_batch_file=true;
              valid_single_file=false;

              var o="";
              var utmpid,fname;
              var i=0;
              batch_files_metadata="";
              $.each(data.split(" "),function(k,url){
                o=o+"<img class='uploader_batch_img' src='"+url+"'>";
                utmpid=url.split("/");
                utmpid=utmpid[utmpid.length-1].split(".")[0];
                fname=batch_files[i++].name.split(".")[0];
                batch_files_metadata=batch_files_metadata+utmpid+","+fname+" ";
              });
              batch_files_metadata=batch_files_metadata.substring(0,batch_files_metadata.length-1);

              $('#uploader_gif').css('display','none');
              $('#uploader_close_btn_overlay').css('display','flex');
              $('.uploader_confirm_btn_overlay').css('display','flex');
              $('.uploader_upload_btn').css('display','initial');
              $('#uploader_batch_preview').html(o);
              $('#uploader_batch_preview').scrollLeft(80);
            } else {
              alert('PNG/JPG - 2MB LIMIT [PNGs please!] (415)');
              $('#uploader_close_btn').click();
            }
          },
          error:function(){
            alert('PNG/JPG - 2MB LIMIT [PNGs please!] (420)');
            $('#uploader_close_btn').click();
          }/*,
          error:function(xhr,thrownError){ alert("error_status:"+String(xhr.status)+"\nerror_msg:"+String(thrownError)); }*/
        });
      }
    } //drop blocked by confirm_window_timestamp
  });
  //prevent other draganddrop defaults
  $(window).on('dragover',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
  });
  $(window).on('dragenter',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
  });
  $(window).on('dragleave',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
  });
  $(window).on('drop',function(evnt){
    evnt.preventDefault();
    evnt.stopPropagation();
  });
});
